package ito.miclase;

public class Ubicacion {

	private float longitud;
     private float latitud;
     private String periodo;
     private float distancia;

     
     public Ubicacion(float longitud, float latitud, String periodo, float distancia) {
		super();
		this.longitud = longitud;
		this.latitud = latitud;
		this.periodo = periodo;
		this.distancia = distancia;
		}

	public float getLongitud() {
		return longitud;
	}

	public float getLatitud() {
		return latitud;
	}

	public String getPeriodo() {
		return periodo;
	}

	public float getDistancia() {
		return distancia;
	}

	@Override
	public String toString() {
		return "Ubicacion [longitud=" + longitud + ", latitud=" + latitud + ", periodo=" + periodo + ", distancia="
				+ distancia + "]";
	}
}
